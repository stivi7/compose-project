const snoowrap = require('snoowrap');

const redditClient = new snoowrap({
    clientId: process.env.REDDIT_CLIENT_ID,
    clientSecret: process.env.REDDIT_CLIENT_SECRET,
    userAgent:  process.env.USER_AGENT,
    refreshToken: process.env.REDDIT_REFRESH_TOKEN
})

module.exports = redditClient;