const r = require('../snoowrap');

const categories = {
    hot: 'getHot',
    best: 'getBest',
    new: 'getNew',
    rising: 'getRising'
}

const getSubredditPosts = async (req, res) => {
    try {
        const { subRedditName } = req.params;
        let { category } = req.query;
        
        if (category && !categories[category]) {
            throw new Error('Category not supported')
        }
        if (!category) {
            category = 'hot';
        }
    
        const response = await r.getSubreddit(subRedditName)[categories[category]]({ amount: 'all' });
    
        res.json(response)
    } catch (error) {
        res.send({ error: error.message })
    }
}

module.exports = {
    getSubredditPosts,
}