const express = require('express');
const cors = require('cors');
const app = express();
require('dotenv').config()

const routes = require('./routes');

const port = process.env.PORT || 3001;

app.use(cors());

routes.map((route) => {
    app[route.method](route.path, route.controller);
})

app.listen(port, () => console.log(`App listening on port ${port}!`))