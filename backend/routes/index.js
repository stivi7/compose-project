const { getSubredditPosts } = require('../controllers/subreddit');


module.exports = [
    {
        path: '/r/:subRedditName',
        method: 'get',
        controller: getSubredditPosts,
    }
]