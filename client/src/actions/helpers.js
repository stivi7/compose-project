export const actionStart = (type, payload) => ({ type, status: 'start', payload});
export const actionSuccess = (type, payload) => ({ type, status: 'success', payload});
export const actionFail = (type, payload) => ({ type, status: 'fail', payload});