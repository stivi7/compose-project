import React  from 'react';
import './App.css';
import reddit from './assets/images/reddit_logo.png'
import Routes from './routes';

import Navigator from './components/Navigator/Presentational';

function App(props) {
  return (
    <div className="App">
      <img className="App-logo" src={reddit} alt="reddit" />
      <Navigator />
      <Routes />
    </div>
  );
}

export default App;
