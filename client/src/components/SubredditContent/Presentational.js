import React, { useState, useEffect } from 'react';
import { Button } from 'antd';
import { useStore, useSelector } from 'react-redux';
import queryString from 'query-string';

import './Styles.css';
import * as initiators from '../../constants/saga_initatiors';
import LoadingScreen from '../LoadingScreen/Presentational';

const SubredditContent = ({ location, match }) => {
  const { search } = location;
  const store = useStore();

  const [imageIndex, setImageIndex] = useState(0);

  const subreddit = useSelector((state) => state.subreddit);
  
  useEffect(() => {
    const subredditName = match.params.subredditName;
    const category = queryString.parse(search).category;
    store.dispatch({
      type: initiators.START_SUBREDDIT_FETCH,
      subredditName,
      category
    });
  }, [match, search, store]);

  const subredditImages =
    subreddit.data &&
    subreddit.data.filter((post) => {
      return (
        post.preview &&
        post.preview.images &&
        post.preview.images[0] &&
        post.preview.images[0].source
      );
    }).map(item => item.preview.images[0].source);    


  const currentImage = subredditImages && subredditImages.length && subredditImages[imageIndex].url;

  console.log(subreddit)

  // Handlers

  const onNextClick = () => {
    if (imageIndex < subredditImages.length - 1) {
      setImageIndex(imageIndex + 1)
    }
  }

  const onPrevClick = () => {
    if (imageIndex > 0) {
      setImageIndex(imageIndex - 1);
    }
  }

  return (
    <div className="subreddit-content">
      <LoadingScreen loading={subreddit.loading} />
      <div className="image-container">
        <img src={currentImage} alt=""/>
      </div>
      <div className="actions-container">
        <Button onClick={onPrevClick}>Prev</Button>
        <Button onClick={onNextClick}>Next</Button>
      </div>
    </div>
  );
};

export default SubredditContent;
