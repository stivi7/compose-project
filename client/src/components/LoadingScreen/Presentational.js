import React from 'react';
import PropTypes from 'prop-types';

import { Spin, Icon } from 'antd';
import './Styles.css';

const LoadingScreen = ({ loading }) => {
  if (loading) {
    return (
        <div className="loading-screen">
          <Spin
            indicator={
              <Icon type="loading" spin style={{ fontSize: 32, color: 'white' }} />
            }
          />
        </div>
      );
  }
  return null;
};

LoadingScreen.propTypes = {
  loading: PropTypes.bool
};

export default LoadingScreen;
