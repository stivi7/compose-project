import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import { Menu } from 'antd';
const { SubMenu } = Menu;

const subMenuStructure = (parent) => [
  {
    title: 'Hot',
    key: `${parent}-hot`
  },
  {
    title: 'New',
    key: `${parent}-new`
  },
  {
    title: 'Best',
    key: `${parent}-best`
  },
  {
    title: 'Rising',
    key: `${parent}-rising`
  }
];

const menuStructure = [
  {
    key: 'wallpaper-0',
    menuTitle: 'Wallpaper',
    subreddit: 'wallpaper',
    subMenu: subMenuStructure('wallpaper')
  },
  {
    key: 'wallpaper-1',
    menuTitle: 'iWallpaper',
    subreddit: 'iWallpaper',
    subMenu: subMenuStructure('iWallpaper')
  },
  {
    key: 'wallpaper-2',
    menuTitle: 'Wallpapers',
    subreddit: 'wallpapers',
    subMenu: subMenuStructure('wallpapers')
  }
];

const Navigator = ({ history }) => {
  const [current, setCurrent] = useState(null);

  const handleClick = (e) => {
    setCurrent(e.key);
  };

  useEffect(() => {
    if (current) {
      const currentParts = current.split('-');
      const [subreddit, category] = currentParts;
      const path = `/r/${subreddit}?category=${category}`;

      history.replace(path);
    }
  }, [current, history]);

  return (
    <Menu mode="horizontal" onClick={handleClick} selectedKeys={[current]}>
      {menuStructure.map((menu) => {
        return (
          <SubMenu title={menu.menuTitle} key={menu.key}>
            {menu.subMenu.map((subMenu) => {
              return <Menu.Item key={subMenu.key}>{subMenu.title}</Menu.Item>;
            })}
          </SubMenu>
        );
      })}
    </Menu>
  );
};

Navigator.propTypes = {
  history: PropTypes.object
};

export default withRouter(Navigator);
