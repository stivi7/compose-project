import { combineReducers } from 'redux';

import subreddit from './subreddit';

const rootReducer = combineReducers({
    subreddit,
})

export default rootReducer;