import * as types from '../constants/action_types';

const initialState = {
    loading: false,
    error: '',
    data: null,
}

const fetchSubredditReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.FETCH_SUBREDDIT: {
            if (action.status === 'start') {
                return {
                    ...state,
                    loading: true,
                }
            }
            if (action.status === 'success') {
                return {
                    ...state,
                    loading: false,
                    data: action.payload,
                }
            }
            if (action.status === 'fail') {
                return {
                    ...state,
                    loading: false,
                    error: action.payload.error,
                }
            }
            return state;
        }
        default:
            return state;
    }
}

export default fetchSubredditReducer;