import { all } from 'redux-saga/effects';
import subredditSaga from './subreddit';

export default function* rootSaga() {
    yield all([
        subredditSaga()
    ])
}