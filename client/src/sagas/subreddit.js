import { call, put, takeEvery } from 'redux-saga/effects'

import { getSubreddit } from '../api/sdk/sub_reddit';
import { actionStart, actionSuccess, actionFail } from '../actions/helpers';
import * as types from '../constants/action_types';
import * as initatiors from '../constants/saga_initatiors';

function* fetchSubreddit(action) {
    try {
        yield put(actionStart(types.FETCH_SUBREDDIT))

        const response = yield call(getSubreddit, {
            subredditName: action.subredditName,
            category: action.category,
        })

        yield put(actionSuccess(types.FETCH_SUBREDDIT, response.data))
    } catch (error) {
        yield put(actionFail(types.FETCH_SUBREDDIT, {
            error: error.message,
        }))
    }
}

function* subredditSaga() {
    yield takeEvery(initatiors.START_SUBREDDIT_FETCH, fetchSubreddit)
}

export default subredditSaga;