import React from 'react';
import { Route, Switch } from 'react-router-dom';

import SubredditContent from './components/SubredditContent/Presentational'

const Routes = () => (
    <Switch>
        <Route path="/r/:subredditName" component={SubredditContent} />
    </Switch>
)

export default Routes;

