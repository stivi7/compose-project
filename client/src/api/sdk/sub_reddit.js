import axios from 'axios';
import { subReddit } from '../endpoints';

export const getSubreddit = (params) => {
    const { subredditName, category } = params;
    return axios({
        method: 'get',
        url: subReddit(subredditName, category),
    })
}