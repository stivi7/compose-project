const HOST = 'http://localhost:3001';

export const subReddit = (subReddit, category) => `${HOST}/r/${subReddit}${category ? `?category=${category}` : ''}`
